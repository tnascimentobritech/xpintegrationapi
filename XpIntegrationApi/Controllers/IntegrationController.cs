﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XpIntegrationApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IntegrationController : ControllerBase
    {
        public async Task<IActionResult> AnbimaApiIntegration()
        {
            return Ok();
        }

        public async Task<IActionResult> ExcelIntegration()
        {
            return Ok();
        }
    }
}
